# The Coding Club Website

## Planning

### Section 1  - Front Page

Images & welcome texts

### Section 2 - About Us

- Small About
- Why o join us?
- What we are doing?


### Section 3 - Feedback/Testimonials

### Section 4 - Footer & Partner Logos

## Sketch 

https://preview.ibb.co/gCpRFb/cc.png

## Bootstrap
### Build responsive, mobile-first projects on the web with the world's most popular front-end component library.
### Using bootstrap to create content column more easy and it automatically responsive.
### class 
    col-lg - is to large screen, 
    col-md - to medium screen for example Ipad, 
    col-sm - is to smaller screen for example Iphone6 
            